import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/sub_screens/portfolio_galary_sub_page.dart';
import 'package:flutter_app/sub_screens/portfolio_tutorial_sub_page.dart';
import 'package:tuple/tuple.dart';

class Portfolio extends StatefulWidget {
  @override
  _PortfolioState createState() => _PortfolioState();
}

class _PortfolioState extends State<Portfolio> {
  final _pages = [
    PortfolioTutorialSubPage(),
    PortfolioGallerySubPage(),
  ];

  int _selectedPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white12,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.blueGrey,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Colors.lightGreenAccent,
              icon: Icon(Icons.video_collection_outlined,),
            label: 'Tutorials',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.image),
            label: 'Gallery',
            backgroundColor: Colors.black,
          ),
        ],
        currentIndex: _selectedPage,
        onTap: (index){
          setState(() {
            _selectedPage = index;
          });
        },
      ),
      body: _pages[_selectedPage],
    );
  }
}

