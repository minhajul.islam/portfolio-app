
import 'package:flutter/material.dart';

import 'portfolio.dart';

class ShopDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(canvasColor: Colors.blueGrey),
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [

            _buildDrawerHeader(context),
            _buildNavigationItemList(context,'Portfolio',Icons.person,),
            _buildDivider(),
            _buildNavigationItemList(context,'About Me',Icons.portrait_rounded),
            _buildDivider(),
            _buildNavigationItemList(context,'Contact',Icons.phone_bluetooth_speaker),
            _buildDivider(),

          ],
        ),
      ),
    );
  }

 UserAccountsDrawerHeader _buildDrawerHeader(BuildContext context) {
    return UserAccountsDrawerHeader(
      accountName: Text('Minhajul Islam',style: TextStyle(fontSize: 18.0,
          backgroundColor: Colors.black),),
      accountEmail: Text('minhajul.islam021@gmail.com',style:TextStyle(fontSize: 15.0,
          backgroundColor: Colors.black) ,),
      currentAccountPicture: GestureDetector(
        onTap: ()=> showDialog(
            context: context,
          child: AlertDialog(
            title: Text('Highlights',style: TextStyle(color: Colors.grey),),
            content: Text('I am Minhajul Islam a flutter developer.I am from Munshiganj and live in dhaka.'
                'I am the servant of Allah Subahanu ta\'la and ummah of prophet Hazrat Mohammad (sm).'
                'I am very proud to be a muslim.',
              style: TextStyle(color: Colors.black,
              fontSize: 20.0),),
            actions: [
              FlatButton(
                  onPressed: ()=> Navigator.of(context).pop(),
                  child: Text('Close',style: TextStyle(color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24.0),),),
            ],
            backgroundColor: Colors.blueGrey,
          )
        ),
        child: CircleAvatar(
          backgroundColor: Colors.black,
          backgroundImage: NetworkImage('https://lh3.googleusercontent.com/a-/AOh14GhukuPBHk7UA99RItbCChQwE54I84paQoVk3hy8Jw=s96-c-rg-br100'),

        ),
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage('https://drscdn.500px.org/photo/110815209/m%3D900/v2?sig=39ccf8d5675a3a3b8b3c4470f9ef493873cda0383cc40b0d4f75d70d78f70379'),
          fit: BoxFit.cover,
        )
      ),
    );
  }

 ListTile _buildNavigationItemList( BuildContext context, String itemTile, IconData icon) {

    return ListTile(
      leading: Icon(icon),
      title: Text(itemTile),
      trailing:Icon(Icons.arrow_right),
      onTap: (){
        Navigator.pop(context);
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Portfolio()));
      },
    );
  }

  _buildDivider() {
   return Divider(
     color: Colors.black,
     thickness: 1.0,
   );
  }
}
