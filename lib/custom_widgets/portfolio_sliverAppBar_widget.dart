import 'package:flutter/material.dart';

class PortfolioSliverAppBar extends StatelessWidget {
  final String _title;
  const PortfolioSliverAppBar(this._title,{
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.blueGrey,
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      expandedHeight: 200,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(_title,style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 22.0,
        ),
        ),
        background: Image.asset('assets/profile/tutorial_image.jpg',fit: BoxFit.cover,)
      ),

    );
  }
}
