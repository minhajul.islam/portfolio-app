import 'package:flutter/material.dart';

import 'portfolio_sliverAppBar_widget.dart';

class PortfolioGalarySubPageWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
        slivers: <Widget> [
          PortfolioSliverAppBar('Gallery'),
        ]
    );
  }

 SliverFillRemaining _buildFixedContentSliverList() {
  return SliverFillRemaining(
    child: Text('This is gallery',style: TextStyle(
      fontSize: 22.0,
      fontWeight: FontWeight.bold,
    ),
    ),
  );
  }
}

