import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/screens/portfolio.dart';
import 'screens/shop_drawer.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Portfolio of Minhajul Islam"),
        backgroundColor: Colors.blueGrey,
      ),
      drawer: ShopDrawer(),
      body:Stack(
        alignment: Alignment.center,
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/profile/home_page.jpg'),
                  fit: BoxFit.fill,
                )
            ),
          ),
          Center(
              child: Positioned(
                top: 0.0,
                bottom: 0.0,
                child: OutlineButton(
                  borderSide: BorderSide(color: Colors.black,width: 3.0,style: BorderStyle.solid),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                      child: Text('Get Me There',style: TextStyle(
                        fontSize: 22.0,
                        color: Colors.white,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                  ),
                    onPressed: (){
                    Navigator.of(context).push(MaterialPageRoute(builder: (context)=>Portfolio()));
                    },
                ),
              ),
            ),

        ],
      )
    );
  }
}
