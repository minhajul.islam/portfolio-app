import 'package:flutter/material.dart';
import 'package:flutter_app/custom_widgets/portfolio_sliverAppBar_widget.dart';
import 'package:tuple/tuple.dart';

class PortfolioTutorialSubPage extends StatelessWidget {
  static const List<Tuple2> tutorials = [
    const Tuple2<String, String>(
      'http://himdeve.com/wp-content/uploads/2020/01/1_1_simple_webview_en_banner.jpg',
      'Flutter Tutorials – #1.1 – First App – Simple WebView',
    ),
    const Tuple2<String, String>(
      'https://himdeve.com/wp-content/uploads/2020/01/1_2_webview_controller_en_banner.jpg',
      'Flutter Tutorials – #1.2 – WebView Controller – Completer, Future Builder, Await – Async',
    ),
    const Tuple2<String, String>(
      'https://himdeve.com/wp-content/uploads/2020/01/1_3_webview_javascript_en_banner.jpg',
      'Flutter Tutorial – #1.3 – WebView – Navigation Controls, Javascript communication',
    ),
    const Tuple2<String, String>(
      'https://himdeve.com/wp-content/uploads/2020/01/1_4_drawer_en_banner_final.jpg',
      'Flutter Tutorials – #1.4 – DRAWER – PageRoute, Navigator, UserAccountsDrawerHeader',
    ),
    const Tuple2<String, String>(
      'https://himdeve.com/wp-content/uploads/2020/02/1_5_sliverappbar_banner_en.jpg',
      'Flutter Tutorials – #1.5 – Sliver App Bar = Collapsing Toolbar',
    ),
    const Tuple2<String, String>(
      'https://himdeve.com/wp-content/uploads/2020/02/1_6_sliverlist_banner_en.jpg',
      'Flutter Tutorials – #1.6 – SliverList',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
            slivers: <Widget> [
              PortfolioSliverAppBar('Tutorials'),
              _buildFixedContentSliverList(),

              SliverList(
                delegate: SliverChildListDelegate([
                  for(var i=0; i<=100; i++)
                    ListTile(
                      title: Text(i.toString()),
                    )
                ]),
              )

            ]
        );
  }



  SliverFixedExtentList _buildFixedContentSliverList(){
   return SliverFixedExtentList(
      itemExtent: 110.0,
      delegate: SliverChildBuilderDelegate(
            (BuildContext context,int index){
          return  _buildSliverList(tutorials[index]);

        },
        childCount: tutorials.length,
      ),
    );
  }

  Card  _buildSliverList(Tuple2 tutorial) {
    return Card(
      margin: EdgeInsets.all(5.0),
      color: Colors.white70,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: Opacity(
              opacity: 0.9,
              child: Image(
                image: NetworkImage(tutorial.item1,scale: 12,),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0,right: 10.0,bottom: 10.0),
              child: Text(tutorial.item2,style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

