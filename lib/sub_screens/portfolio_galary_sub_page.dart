import 'package:flutter/material.dart';

class PortfolioGallerySubPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
        child: _buildGridImageContent()
    );
  }

  GridView _buildGridImageContent() {
    return GridView.count(
      crossAxisCount: 2,
      mainAxisSpacing: 10.0,
      crossAxisSpacing: 5.0,
      children: <Widget>[
        _buildImageWidget('assets/images/baby_girl.jpg'),
        _buildImageWidget('assets/images/boat_nature.jpg'),
        _buildImageWidget('assets/images/boys_cow.jpg'),
        _buildImageWidget('assets/images/child_with_grandma.jpg'),
        _buildImageWidget('assets/images/boys_with_dslr.jpg'),
        _buildImageWidget('assets/images/dad_son.jpg'),
        _buildImageWidget('assets/images/dog_girl.jpg'),
        _buildImageWidget('assets/images/flying_bird.jpg'),
        _buildImageWidget('assets/images/football.jpg'),
        _buildImageWidget('assets/images/mashroom.jpg'),
        _buildImageWidget('assets/images/moyurakkhi_hair.jpg'),
        _buildImageWidget('assets/images/play_water.jpg'),
        _buildImageWidget('assets/images/snow_girl.jpg'),
      ],
    );
  }

  Image _buildImageWidget(String imagePath){
    return Image.asset(imagePath,fit: BoxFit.cover);
  }

}